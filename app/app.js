"use strict";
(function () {
   var app = angular.module("userSearchApp", []);
   app.controller('searchController', function($scope, $http) {
     $http.get("data.js").success(function(data){
     	$scope.users =  data;
     });
   });
   app.directive('searchForm', function() {
       return {
           restrict: 'E',
           templateUrl: 'views/search-form.html'
       };
   });
   app.directive('userList', function() {
       return {
           restrict: 'E',
           templateUrl: 'views/user-list.html'
       };
   });   
}());