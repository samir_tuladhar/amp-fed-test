describe("User Search", function () {
    beforeEach(module('userSearchApp'));
 
    describe("searchController", function () {
 
        var scope, httpBackend;
        beforeEach(inject(function ($rootScope, $controller, $httpBackend, $http) {
            scope = $rootScope.$new();
            httpBackend = $httpBackend;
            httpBackend.when("GET", "data.js").respond([{}, {}, {},{},{},{}]);
            $controller('searchController', {
                $scope: scope,
                $http: $http
            });
        }));
 
        it("Should have 6 users", function () {
            httpBackend.flush();
            chai.assert.equal(scope.users.length, '6');
        });
    });
});
