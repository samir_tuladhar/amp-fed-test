module.exports = function(grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),
        sassFiles: 'app/**/*.scss',
		
        sass: {
            dist: {
                files: {
                    'build/site.css': 'app/site.scss'
                }
            }
        }, 
        
        karma: {
			options: {
				configFile: 'karma.conf.js'
			},
			unit: {
				singleRun: true
			},
			continuous: {
				background: true
			}
        },

        watch: {
            sass: {
                tasks: ['sass'],
                files: 'app/**/*.scss'
            },
            karma: {
            	files: ['test/unit/*.js'] 
            }

        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-karma');

    grunt.registerTask('dev', ['default', 'watch']);
    grunt.registerTask('default', ['sass']);
    grunt.registerTask('unit-test', ['karma:continuous:start', 'watch:karma']);

};